package main

import (
	"github.com/akrylysov/algnhsa"
	"github.com/sirupsen/logrus"
	"github.com/vrischmann/envconfig"
)

func main() {
	conf := Config{}
	if err := envconfig.Init(&conf); err != nil {
		panic(err)
	}

	if level, err := logrus.ParseLevel(conf.LogLevel); err == nil {
		logrus.SetLevel(level)
	}
	srv, err := NewServer(conf)
	if err != nil {
		panic(err)
	}
	srv.routes()
	go srv.machineEventSubscr.Subscribe()

	algnhsa.ListenAndServe(srv, nil)
}
