package main

import (
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"net/http"
)

func xrayHandler(name string) func(http.Handler) http.Handler {
	sn := xray.NewFixedSegmentNamer(name)
	return func(next http.Handler) http.Handler {
		return xray.Handler(sn, next)
	}
}

func stripPrefix(prefix string) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.StripPrefix(prefix, h)
	}
}

const (
	MachineIDParam = "machine_id"
	CompanyIDParam = "company_id"
	)

func (srv *server) routes() {
	r := chi.NewRouter()

	r.Use(stripPrefix(srv.config.BasePath))
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(xrayHandler(srv.config.ServiceName))

	r.Post("/sns/machine_events", srv.machineEventHandler)

	r.With(Timestamp).Group(func(r chi.Router) {
		r.Get("/all/{"+CompanyIDParam+"}/{timestamp}", srv.calculateForAllMachines)
	})

	r.Route("/{"+MachineIDParam+"}", func(r chi.Router) {
		r.Route("/config", func(r chi.Router) {
			r.Post("/", srv.postConfig())
			r.Get("/", srv.getConfig())
			r.Delete("/", srv.deleteConfig())
		})

		r.Group(func(r chi.Router) {
			r.Use(KPIConfigCtx(srv.configRepo))
			r.Put("/refresh", srv.refreshNow)
			r.Get("/details", srv.calculateDetails)

			r.With(Timestamp).Group(func(r chi.Router) {
				r.Get("/values/{timestamp}", srv.calculate)

			})
		})
	})

	srv.Router = r
}
