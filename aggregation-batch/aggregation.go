package main

import "bitbucket.org/to-increase/kpi-breakdown-api/types"

func aggregate(mid string, ts int64, results []QueryResult) types.TimestampedBreakdownAggregate {
	res := types.TimestampedBreakdownAggregate{
		Timestamp: ts,
		MachineID: mid,
	}
	for _, qr := range results {
		if qr.Value == 1 {
			res.Uptime += qr.Seconds
		}
		if qr.Value > 1 {
			res.Downtime += qr.Seconds
			if qr.Starttime != float64(ts) { //If the downtime started at the start of the day, it was already counted yesterday
				res.Breakdowns++
			}
		}
	}
	return res
}
