package machines

import (
	"bitbucket.org/to-increase/go-sns"
	"bitbucket.org/to-increase/go-sns/events/kpis"
	"bitbucket.org/to-increase/go-sns/events/machine"
	"bitbucket.org/to-increase/go-sns/subscriber"
	"bitbucket.org/to-increase/kpi-breakdown-api/repository"
	"context"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/go-openapi/errors"
	"net/http"
)

func New(r repository.ConfigRepo, a repository.AggregationRepo, p publisher.Publisher) subscriber.EventHandler {
	s := &machineEventSubscription{
		MachineEventSubscription: &machineevents.MachineEventSubscription{},
		repo: r,
		aggregationRepo: a,
		kpiEventPublisher: p,
	}

	s.OnMachineDeleted(s.handleMachineDeletedEvent)
	s.OnCompanyDeleted(s.handleCompanyDeletedEvent)
	return s

}

// swagger:operation POST /sns/machine_events machineEvents machineEventsHandler
//
// ---
//
// parameters:
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-breakdown-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the config has been posted
type machineEventSubscription struct {
	*machineevents.MachineEventSubscription
	repo repository.ConfigRepo
	aggregationRepo repository.AggregationRepo
	kpiEventPublisher publisher.Publisher
}

func (s *machineEventSubscription) handleMachineDeletedEvent(c context.Context, evt machineevents.MachineDeletedEvent) error {
	return s.delete(c, evt.ID)
}

func (s *machineEventSubscription) handleCompanyDeletedEvent(c context.Context, evt machineevents.CompanyDeletedEvent) error {
	for _, m := range evt.Machines {
		s.delete(c, m)
	}
	return nil
}

func (s *machineEventSubscription) delete(c context.Context, mid string) error {
	conf, err := s.repo.Get(c, mid)
	if err != nil {
		return err
	}
	if conf == nil {
		return nil
	}

	if err := s.repo.Delete(c, mid); err != nil {
		return err
	}

	xray.CaptureAsync(c, "delete aggregated data", func(ctx context.Context) error {
		go func() {
			s.aggregationRepo.Delete(ctx, mid)
		}()
		return nil
	})

	MTTRerror := s.kpiEventPublisher.Publish(c, kpis.KpiDeletedEvent{MachineID:mid, KPI: "MTTR"})
	MTBFerror := s.kpiEventPublisher.Publish(c, kpis.KpiDeletedEvent{MachineID:mid, KPI: "MTBF"})

	if MTTRerror != nil && MTBFerror != nil{
		return errors.New(http.StatusInternalServerError, "Failed to send MTTR and MTBF messages. MTTR error: %+v, MTBF error: %+v", MTTRerror, MTBFerror)
	}
	if MTTRerror != nil {
		return MTTRerror
	}
	return MTBFerror
}