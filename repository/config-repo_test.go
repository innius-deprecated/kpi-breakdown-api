package repository

import (
	"testing"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"context"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/go-trn/uuid"
)

func localDynamo() dynamodbiface.DynamoDBAPI {
	awsConfig := &aws.Config{}
	sess, _ := session.NewSession(awsConfig.WithEndpoint("http://localhost:8000").WithRegion("us-east-1"))
	return dynamodb.New(sess)
}

func TestStoreConfig(t *testing.T) {
	ctx := context.Background()

	repo := NewConfigRepo(localDynamo(), "config-table").(*configRepo)
	repo.model.CreateTable(ctx)

	mtrn := trn.NewMachine(uuid.NewV4().String())

	config := types.Config{
		MachineID: mtrn.String(),
	}

	if assert.NoError(t, repo.Save(ctx, &config)) {
		cfg, err := repo.Get(ctx, mtrn.String())
		assert.NoError(t, err)
		assert.NotNil(t, cfg)
		assert.Equal(t, config, *cfg)
	}

	t.Run("delete the config", func(t *testing.T) {
		assert.NoError(t, repo.Delete(ctx, mtrn.String()))
		cfg, err := repo.Get(ctx, mtrn.String())
		assert.NoError(t, err)
		assert.Nil(t, cfg)
	})
}
