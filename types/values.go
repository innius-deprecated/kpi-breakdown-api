package types

import "net/http"

type TimestampedBreakdownAggregate struct {
	MachineID  string `json:"machine_id,omitempty"`
	Timestamp  int64  `json:"timestamp"`
	Uptime     int64  `json:"uptime"`
	Downtime   int64  `json:"downtime"`
	Breakdowns int    `json:"breakdowns"`
}

func (t TimestampedBreakdownAggregate) HasData() bool {
	return t.Uptime > 0 || t.Downtime > 0
}

// BreakdownResponse contains values from MTTR and MTBF
//
// A innius MTTR/MTBF kpi value
//
// swagger:model BreakdownResponse
type BreakdownResponse struct {
	MachineId  string       `json:"machine_id"`
	FromDate   int64        `json:"from_date"`
	ToDate     int64        `json:"to_date"`
	MTTR       BreakdownKPI `json:"mttr"`
	MTBF       BreakdownKPI `json:"mtbf"`
	Uptime     int64        `json:"uptime"`
	Downtime   int64        `json:"downtime"`
	Breakdowns int          `json:"breakdowns"`
}

func (b *BreakdownResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

type BreakdownKPI struct {
	Value     float64  `json:"value"`
	Status    int      `json:"status"`
	Caution   *float64 `json:"caution_level"`
	Emergency *float64 `json:"emergency_level"`
	Trend     int      `json:"trend"`
}

type BreakdownValue struct {
	MTTR       float64 `json:"mttr"`
	MTBF       float64 `json:"mtbf"`
	Uptime     int64   `json:"uptime"`
	Downtime   int64   `json:"downtime"`
	Breakdowns int     `json:"breakdowns"`
}

type TimestampedBreakdownValue struct {
	BreakdownValue
	Timestamp int64 `json:"timestamp"`
}

// BreakdownDetailsResponse contains daily/hourly values from MTTR and MTBF
//
// A series of innius MTTR/MTBF timestamped kpi values
//
// swagger:model BreakdownDetailsResponse
type BreakdownDetailsResponse struct {
	FromDate int64                       `json:"from_date"`
	ToDate   int64                       `json:"to_date"`
	Values   []TimestampedBreakdownValue `json:"values"`
}

func (b BreakdownDetailsResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
