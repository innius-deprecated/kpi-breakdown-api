package main

import (
	"github.com/stretchr/testify/assert"
	"github.com/vrischmann/envconfig"
	"os"
	"testing"
)

func TestParseConfig(t *testing.T) {
	os.Setenv("REPOSITORY_DYNAMODB_TABLENAME", "table-1")
	os.Setenv("AGGREGATION_DYNAMODB_TABLENAME", "table-2")
	os.Setenv("ENVIRONMENT_NAME", "foo")
	os.Setenv("SERVICE_NAME", "svcname")
	os.Setenv("MESSAGING_EVENT_QUEUE", "msg_queue")
	conf := Config{}
	assert.NoError(t, envconfig.Init(&conf))
}
