Description: |
  CI/CD pipeline for the innius Breakdown api

Outputs:
  PipelineUrl:
    Value:
      Fn::Sub: https://console.aws.amazon.com/codepipeline/home?region=${AWS::Region}#/view/${Pipeline}
Parameters:
  ArtifactBucket:
    Description: The S3 bucket for storing the codepipeline artifacts
    Type: String
    Default: innius-ci-codepipelineartifactbucket-pk9zrjemn5iy
  CloudFormationExecutionRoleArn:
    Description: the arn of the IAM role for performing cloudformation deployments
    Type: String
    Default: arn:aws:iam::831844703282:role/innius-ci-us-east-1-CloudFormationExecutionRole
  CodeBuildServiceRoleArn:
    Description: The arn of the IAM role for executing the codebuild
    Type: String
    Default: arn:aws:iam::831844703282:role/innius-ci-CodeBuildServiceRole-C3QF934S2Z21
  CodePipelineServiceRoleArn:
    Description: The arn of the IAM role for executing the codepipeline
    Type: String
    Default: arn:aws:iam::831844703282:role/innius-ci-CodePipelineServiceRole-1KSW6FFYRRL4M
  CodebuildImage:
    Description: The Docker image identifier that the build environment uses
    Type: String
    Default: aws/codebuild/golang:1.11
  CopyArtifactsToS3BuildProject:
    Description: The codebuild project to copy artifacts to target s3 bucket
    Type: String
    Default: CopyArtifactsToS3
  ECRCodeBuildProject:
    Description: The codebuild project for building / pushing ECR container images
    Type: String
    Default: ECRPushCodeBuildProject
  GitBranch:
    Default: master
    Type: String
  ServiceName:
    Type: String
    Default: kpi-breakdown-api
Resources:
  CodeBuildProject:
    Properties:
      Artifacts:
        Location:
          Ref: ArtifactBucket
        Type: S3
      Environment:
        ComputeType: BUILD_GENERAL1_SMALL
        EnvironmentVariables:
        - Name: AWS_REGION
          Value:
            Ref: AWS::Region
        - Name: INNIUS_STACK_NAME
          Value: codebuild
        - Name: CODECOV_TOKEN
          Type: PARAMETER_STORE
          Value:
            Ref: CodecovTokenParameter
        - Name: SERVICE_NAME
          Value:
            Ref: ServiceName
        Image:
          Ref: CodebuildImage
        PrivilegedMode: true
        Type: LINUX_CONTAINER
      Name:
        Ref: ServiceName
      ServiceRole:
        Ref: CodeBuildServiceRoleArn
      Source:
        Location:
          Fn::GetAtt:
          - GitRepository
          - CloneUrlHttp
        Type: CODECOMMIT
      Tags:
      - Key: ServiceName
        Value:
          Ref: ServiceName
    Type: AWS::CodeBuild::Project
  CodeCommitCodeBuildEventRule:
    DependsOn: Pipeline
    Properties:
      Description: codecommit rule which triggers codebuild for the selected commit
      EventPattern:
        Fn::Sub: "{\n  \"source\": [\n    \"aws.codecommit\"\n  ],\n  \"detail-type\"\
          : [\n    \"CodeCommit Repository State Change\"\n  ],\n  \"resources\":\
          \ [\n    \"arn:aws:codecommit:us-east-1:831844703282:${ServiceName}\"\n\
          \  ],\n  \"detail\": {\n    \"event\": [\n      \"referenceCreated\",\n\
          \      \"referenceUpdated\"\n    ],\n    \"referenceType\": [\n      \"\
          branch\"\n    ]\n  }\n} \n"
      Targets:
      - Arn:
          Fn::Sub: arn:aws:codebuild:${AWS::Region}:${AWS::AccountId}:project/${ServiceName}
        Id:
          Fn::Sub: codebuild-${ServiceName}
        InputTransformer:
          InputPathsMap:
            commitId: $.detail.commitId
          InputTemplate: '{ "sourceVersion" : <commitId>}'
        RoleArn:
          Ref: CodePipelineServiceRoleArn
    Type: AWS::Events::Rule
  CodeCommitEventRule:
    DependsOn: Pipeline
    Properties:
      Description: Amazon CloudWatch Events rule to automatically start your pipeline
        when a change occurs in the AWS CodeCommit source repository and branch
      EventPattern:
        Fn::Sub: "{\n  \"source\": [\n    \"aws.codecommit\"\n  ],\n  \"detail-type\"\
          : [\n    \"CodeCommit Repository State Change\"\n  ],\n  \"resources\":\
          \ [\n    \"arn:aws:codecommit:us-east-1:831844703282:${ServiceName}\"\n\
          \  ],\n  \"detail\": {\n    \"event\": [\n      \"referenceCreated\",\n\
          \      \"referenceUpdated\"\n    ],\n    \"referenceType\": [\n      \"\
          branch\"\n    ],\n    \"referenceName\": [\n      \"master\"\n    ]\n  }\n\
          } \n"
      Targets:
      - Arn:
          Fn::Sub: arn:aws:codepipeline:${AWS::Region}:${AWS::AccountId}:${ServiceName}
        Id:
          Fn::Sub: codepipeline-${ServiceName}
        RoleArn:
          Ref: CodePipelineServiceRoleArn
    Type: AWS::Events::Rule
  CodecovTokenParameter:
    Properties:
      Description: the codecov.io token for this service
      Name:
        Fn::Sub: /codebuild/${ServiceName}/codecov_token
      Type: String
      Value: get your token from codecov.io
    Type: AWS::SSM::Parameter

  GitRepository:
    Properties:
      RepositoryName:
        Ref: ServiceName
      Triggers:
        - Name: deployment
          DestinationArn: "arn:aws:lambda:us-east-1:831844703282:function:codecommit-deployment-trigger"
          Events:
            - all
    Type: AWS::CodeCommit::Repository

  Pipeline:
    Properties:
      ArtifactStore:
        Location:
          Ref: ArtifactBucket
        Type: S3
      Name: !Ref ServiceName
      RoleArn: !Ref CodePipelineServiceRoleArn
      Stages:
      - Name: Source
        Actions:
        - ActionTypeId:
            Category: Source
            Owner: AWS
            Provider: CodeCommit
            Version: 1
          Configuration:
            BranchName:
              Ref: GitBranch
            RepositoryName:
              Fn::GetAtt:
              - GitRepository
              - Name
          Name: App
          OutputArtifacts:
          - Name: App
          RunOrder: 1
      - Name: Build
        Actions:
        - Name: CodeBuild
          ActionTypeId:
            Category: Build
            Owner: AWS
            Provider: CodeBuild
            Version: 1
          Configuration:
            ProjectName:
              Ref: CodeBuildProject
          InputArtifacts:
          - Name: App
          OutputArtifacts:
          - Name: BuildOutput
          RunOrder: 1
      - Name: Deploy
        Actions:
        - Name: generate-changeset
          ActionTypeId:
            Category: Deploy
            Owner: AWS
            Version: 1
            Provider: CloudFormation
          Configuration:
            ChangeSetName: pipeline-changeset
            ActionMode: CHANGE_SET_REPLACE
            StackName: !Sub "chair-${ServiceName}"
            Capabilities: CAPABILITY_NAMED_IAM
            ParameterOverrides: !Sub |
                { "ServiceName" : "${ServiceName}",
                  "EnvironmentName" : "chair",
                  "MessagingQueueUrl" : "/chair/messaging-service-v2/domain_event_queue_name",
                  "KpiEventSNSTopic": "/chair/kpi-service/events_sns_topic",
                  "MachineEventSNSTopic": "/chair/machine-service/events_sns_topic",
                  "Tag" : { "Fn::GetParam" : [ "BuildOutput", "build.json", "tag" ] }
                }
            TemplatePath: BuildOutput::parsed-template.yml
            RoleArn: !Ref CloudFormationExecutionRoleArn
          InputArtifacts:
            - Name: App
            - Name: BuildOutput
          RunOrder: 1
        - Name: execute-changeset
          ActionTypeId:
            Category: Deploy
            Owner: AWS
            Version: 1
            Provider: CloudFormation
          Configuration:
            ActionMode: CHANGE_SET_EXECUTE
            ChangeSetName: pipeline-changeset
            StackName: !Sub "chair-${ServiceName}"
          InputArtifacts: []
          RunOrder: 2
      - Name: CopyBuildArtifacts
        Actions:
        - ActionTypeId:
            Category: Build
            Owner: AWS
            Provider: CodeBuild
            Version: 1
          Configuration:
            ProjectName:
              Ref: CopyArtifactsToS3BuildProject
          InputArtifacts:
          - Name: BuildOutput
          Name: CopyArtifactsToS3
          OutputArtifacts: []
          RunOrder: 1

    Type: AWS::CodePipeline::Pipeline
  Repository:
    Properties:
      RepositoryName:
        Ref: ServiceName
    Type: AWS::ECR::Repository