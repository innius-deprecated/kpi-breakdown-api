package main

import (
	connection "bitbucket.org/to-increase/go-database-connection"
	"bitbucket.org/to-increase/go-database-connection/redshift"
	localreshift "bitbucket.org/to-increase/go-database-connection/redshift/local"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"context"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"time"
	)

const (
	RedshiftFormat = "2006-01-02 15:04:05"
)

type QuerySystem interface {
	Query(ctx context.Context, config types.Config, start, end int64) ([]QueryResult, error)
}

type querySystem struct {
	redshift *sqlx.DB
	prepared *sqlx.NamedStmt
}

func newMockQuerySystem() (QuerySystem, error) {
	conn, err := localreshift.NewLocalDatabase()
	if err != nil {
		return nil, err
	}
	rs, err := connection.Open(conn)
	if err != nil {
		return nil, err
	}

	stmt, err := rs.PrepareNamed(breakdownQuery)
	return &querySystem{redshift: rs, prepared: stmt}, err
}

func NewQuerySystem(secret string) (QuerySystem, error) {
	conn, err := redshift.New(secret)
	if err != nil {
		return nil, err
	}
	rs, err := connection.Open(conn)
	if err != nil {
		return nil, err
	}
	stmt, err := rs.PrepareNamed(breakdownQuery)
	return &querySystem{redshift: rs, prepared: stmt}, err
}

func (q *querySystem) Query(ctx context.Context, config types.Config, start, end int64) ([]QueryResult, error) {
	params := QueryParams{
		Trn:       config.MachineID,
		Starttime: time.Unix(start, 0).UTC().Format(RedshiftFormat),
		Endtime:   time.Unix(end, 0).UTC().Format(RedshiftFormat),
		Status:    config.StatusSensor,
	}
	result := []QueryResult{}
	err := q.prepared.SelectContext(ctx, &result, params)
	return result, err
}

type QueryParams struct {
	Starttime string
	Endtime   string
	Trn       string
	Status    string
}

const breakdownQuery = `
	 WITH changes AS (
		SELECT event_timestamp, sensor_reading_value AS current_value, LAG(sensor_reading_value) OVER (ORDER BY event_timestamp) AS previous_value
		FROM f_sensor_log WHERE
		machine_id = :trn AND sensor_id = :status AND
		event_timestamp BETWEEN :starttime AND :endtime),
	value_before AS (
		SELECT event_timestamp, sensor_reading_value FROM f_sensor_log WHERE
		machine_id = :trn AND sensor_id = :status AND
		event_timestamp < :starttime 
		ORDER by event_timestamp DESC LIMIT 1
		),
	starts AS (
		(SELECT CAST(:starttime AS TIMESTAMP) as event_timestamp, sensor_reading_value FROM value_before)		
		UNION
		(SELECT event_timestamp, current_value AS sensor_reading_value FROM changes WHERE previous_value IS NULL OR current_value != previous_value)
		ORDER BY event_timestamp
		),
	ends AS (SELECT sensor_reading_value AS value, event_timestamp as starttime,
		COALESCE( LEAD(event_timestamp) OVER (ORDER BY event_timestamp), :endtime ) AS endtime
		FROM starts)
	SELECT value, DATE_PART('epoch', starttime) AS timestamp, DATE_PART('epoch', endtime) - DATE_PART('epoch', starttime) AS seconds FROM ends	
	;	
`

type QueryResult struct {
	Starttime float64 `db:"timestamp"`
	Value     float64 `db:"value"`
	Seconds   int64 `db:"seconds"`
}
