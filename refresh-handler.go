package main

import (
	"net/http"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ecs"
	"github.com/pkg/errors"
	"log"
	"context"
)

// swagger:operation PUT /{machine_id}/refresh breakdowns refreshKPI
//
// refresh MTTR and MTBF
//
// Recalculate the MTTR and MTBF values for this machine for today
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-breakdown-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the calculation is started
// security:
// - sigv4: []

// swagger:operation OPTIONS /{machine_id}/refresh breakdowns refreshKPI
//
// ---
//
// responses:
//   '200':
//     description: "ok"
//     headers:
//       Access-Control-Allow-Origin:
//         type: "string"
//       Access-Control-Allow-Methods:
//         type: "string"
//       Access-Control-Allow-Headers:
//         type: "string"
// x-amazon-apigateway-integration:
//   responses:
//     default:
//       statusCode: "200"
//       description: ""
//       responseParameters:
//         method.response.header.Access-Control-Allow-Methods: "'PUT'"
//         method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
//         method.response.header.Access-Control-Allow-Origin: "'*'"
//   passthroughBehavior: "when_no_match"
//   requestTemplates:
//     application/json: "{\"statusCode\": 200}"
//   type: "mock"
func (srv *server) refreshNow(w http.ResponseWriter, r *http.Request) {
	mid := GetConfigFromContext(r.Context()).MachineID

	if err := srv.refresh(r.Context(), mid); err != nil {
		log.Printf("could not start ecs task %+v", err)
		http.Error(w, "could not refresh kpi", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (srv *server) refresh(c context.Context, machineID string) error {
	containerName := aws.String(srv.config.AggregateBatch.ContainerName)
	cluster := aws.String(srv.config.EnvironmentName)
	taskDefinition := aws.String(srv.config.AggregateBatch.TaskDefinition)

	instances, err := srv.ecsClient.ListContainerInstancesWithContext(c, &ecs.ListContainerInstancesInput{
		Cluster:    cluster,
		MaxResults: aws.Int64(10),
	})
	if err != nil {
		return errors.Wrap(err, "could not list ecs container instances")
	}
	containerInstances := make([]*string, len(instances.ContainerInstanceArns))
	for i := range instances.ContainerInstanceArns {
		containerInstances[i] = instances.ContainerInstanceArns[i]
	}
	out, err := srv.ecsClient.StartTaskWithContext(c, &ecs.StartTaskInput{
		Cluster:            cluster,
		TaskDefinition:     taskDefinition,
		ContainerInstances: containerInstances,
		StartedBy:          aws.String(srv.config.ServiceName),
		Overrides: &ecs.TaskOverride{
			ContainerOverrides: []*ecs.ContainerOverride{
				{
					Name: containerName,
					Environment: []*ecs.KeyValuePair{
						{Name: aws.String("MACHINE_ID"), Value: aws.String(machineID)},
					},
				},
			},
		},
	})
	if err != nil {
		return err
	}
	if len(out.Failures) > 0 {
		return errors.New(out.Failures[0].String())
	}
	return nil
}
