package main

import (
	"bitbucket.org/to-increase/go-event-queue"
	"bitbucket.org/to-increase/kpi-breakdown-api/repository"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/pkg/errors"
	"log"
	"time"
)

var qs QuerySystem
var aggregationRepo repository.AggregationRepo
var messaging Messaging

func handler(c context.Context, config Config) error {
	sess, err := session.NewSession()
	if err != nil {
		log.Fatalf("could not create aws session: %+v", err)
	}

	dyndb := dynamodb.New(sess)
	xray.AWS(dyndb.Client)
	configRepo := repository.NewConfigRepo(dyndb, config.Repository.DynamoDB.TableName)
	aggregationRepo = repository.NewAggregationRepo(dyndb, config.Aggregation.DynamoDB.TableName)

	qb := event_queue.NewDomainEventQueueBuilder().WithQueueUrl(config.Messaging.EventQueue)
	messagingQueue, err := qb.WithSession(sess).Build()
	if err != nil {
		log.Fatalf("could build messaging queue: %+v", err)
	}
	messaging = Messaging{Queue: messagingQueue, Mute: config.Mute}

	qs, err = NewQuerySystem(fmt.Sprintf("/%s/%s/redshift", config.EnvironmentName, config.ServiceName))
	if err != nil {
		log.Fatalf("Error connection to database: %+v", err)
		return errors.Wrap(err, "Error connecting to database")
	}

	var configs []types.Config

	if config.MachineID != "" {
		kpi, err := configRepo.Get(c, config.MachineID)
		if err != nil {
			return errors.Wrapf(err, "could not get config %+v", kpi)
		}
		if kpi == nil {
			return errors.Wrapf(err, "no config defined for %s", config.MachineID)
		}
		configs = []types.Config{*kpi}
	} else {
		allConfigs, err := configRepo.GetAllConfigs(c)
		configs = allConfigs
		if err != nil {
			return errors.Wrap(err, "error reading all configs")
		}
	}

	log.Printf("Starting aggregation for %v configs", len(configs))
	successes := 0
	for i, kpiConfig := range configs {
		log.Printf("Running machine %v (%v / %v)", kpiConfig.MachineID, i+1, len(configs))

		calculationStart, calculationEnd := getCalculationDates(kpiConfig, config)
		if calculationEnd < calculationStart {
			continue
		}

		err := handleMachine(c, kpiConfig, calculationStart, calculationEnd)
		if err != nil {
			xray.AddError(c, err)
			log.Printf("error handling machine %+v; %+v", kpiConfig.MachineID, err)
			continue
		}
		configRepo.UpdateLastUpdatedTimestamp(c, kpiConfig.MachineID, config.CalculationDate)
		successes++
	}
	log.Printf("MTTR/MTBF aggregation for %v complete, worked %v configs", time.Unix(config.CalculationDate, 0).String(), successes)
	return nil
}

func handleMachine(ctx context.Context, kpiConfig types.Config, start, end int64) error {
	for ts := start; ts < end; ts += 86400 {
		endtime := ts + 86400
		if endtime > end {
			endtime = end
		}
		err := aggregateMachineAt(ctx, kpiConfig, ts, endtime)
		if err != nil {
			return err
		}
	}
	return nil
}

func aggregateMachineAt(ctx context.Context, kpiConfig types.Config, timestamp int64, endtime int64) error {
	dayStart := startOfDay(timestamp)

	queryResults, err := qs.Query(ctx, kpiConfig, dayStart, endtime)
	if err != nil {
		return errors.Wrapf(err, "could not query values for %+v", kpiConfig)
	}
	aggr := aggregate(kpiConfig.MachineID, dayStart, queryResults)

	if err := aggregationRepo.Store(ctx, aggr); err != nil {
		return errors.Wrapf(err, "could not store aggregated values for %+v", kpiConfig.MachineID)
	}
	if err := messaging.sendMessages(ctx, messages(kpiConfig, aggr)); err != nil {
		log.Printf("Could not send message(s) for %v: %+v", kpiConfig.MachineID, err)
	}
	return nil
}
