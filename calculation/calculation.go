package calculation

import (
	"bitbucket.org/to-increase/kpi-breakdown-api/repository"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"context"
	)

func Calculate(c context.Context, aggrRepo repository.AggregationRepo, mid string, timestamp int64) (types.BreakdownValue, error) {
	aggregatedValues, err := aggrRepo.Get(c, mid, timestamp)
	return AsValues(aggregatedValues), err
}

func AsValues(agg *types.TimestampedBreakdownAggregate) types.BreakdownValue {
	if agg == nil {
		return types.BreakdownValue{}
	}
	mttr, mtbf := 0.0, 0.0
	if agg.Breakdowns > 0 {
		mttr = float64(agg.Downtime) / float64(agg.Breakdowns)
		mtbf = float64(agg.Uptime) / float64(agg.Breakdowns)
	}
	return types.BreakdownValue{
		MTBF:       mtbf,
		MTTR:       mttr,
		Uptime:     agg.Uptime,
		Downtime:   agg.Downtime,
		Breakdowns: agg.Breakdowns,
	}
}

func CalculateForAllMachines(c context.Context, q repository.AggregationRepo, company string, end int64) (map[string]types.BreakdownValue, error) {
	values, err := q.Getall(c, company, end)
	if err != nil {
		return nil, err
	}
	resp := map[string]types.BreakdownValue{}
	for _, aggregate := range values {
		resp[aggregate.MachineID] = AsValues(&aggregate)
	}
	return resp, nil
}

func CalculateDetails(c context.Context, aggrRepo repository.AggregationRepo, mid string, from, to int64) ([]types.TimestampedBreakdownValue, error) {
	aggregates, err := aggrRepo.GetDetails(c, mid, from, to)
	if err != nil {
		return nil, err
	}
	values := []types.TimestampedBreakdownValue{}
	for i := range aggregates {
		val := AsValues(&aggregates[i])
		values = append(values, types.TimestampedBreakdownValue{BreakdownValue: val, Timestamp: aggregates[i].Timestamp})
	}
	return values, nil
}
