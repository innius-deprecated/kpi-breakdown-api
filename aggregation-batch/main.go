package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-xray-sdk-go/strategy/exception"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/vrischmann/envconfig"
	"log"
	"time"
)

type Options struct {
	Date      int64
	Timeout   time.Duration
	MachineID string
}

func main() {
	conf := Config{}
	if err := envconfig.Init(&conf); err != nil {
		panic(err)
	}
	if conf.CalculationDate < 0 {
		conf.CalculationDate = time.Now().Unix()
	}
	efs, _ := exception.NewDefaultFormattingStrategy()
	ip, _ := localIPV4Address()

	xray.Configure(xray.Config{
		ExceptionFormattingStrategy: efs,
		DaemonAddr:                  fmt.Sprintf("%s:2000", ip),
	})

	ctx, seg := xray.BeginSegment(context.Background(), conf.ServiceName)
	seg.AddAnnotation("stack", conf.EnvironmentName)

	log.Printf("Starting aggregation with config %+v", conf)
	seg.Close(handler(ctx, conf))
}
