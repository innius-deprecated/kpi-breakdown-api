package main

import (
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/go-trn/uuid"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"time"
	"bitbucket.org/to-increase/kpi-breakdown-api/calculation"
)

func TestCalculateKPIs(t *testing.T) {
	ctx := context.Background()

	srv := newTestServer(uuid.NewV4().String())
	mtrn := trn.NewMachine(srv.CompanyID()).String()

	assert.NoError(t, srv.configRepo.Save(context.Background(), &types.Config{
		MachineID: mtrn,
		ConfigDTO: types.ConfigDTO{
			StatusSensor: "status_sensor",
		},
	}))

	day := int64(86400)

	t.Run("Post some values", func(t *testing.T) {
		for i := 0; i < 7; i++ {
			agg := types.TimestampedBreakdownAggregate{
				Timestamp:  int64(i) * day,
				MachineID:  mtrn,
				Breakdowns: 1,
				Downtime:   100,
				Uptime:     200,
			}
			assert.NoError(t, srv.aggrRepo.Store(ctx, agg))
		}
	})

	t.Run("Get MTTR & MTBF", func(t *testing.T) {
		path := fmt.Sprintf("/%s/values/%d", mtrn, 6*day)

		ok, rec := srv.get(t, path, http.StatusOK)
		assert.True(t, ok)
		assert.NotNil(t, rec)

		resp := types.BreakdownResponse{}
		assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &resp))
		assert.EqualValues(t, 7, resp.Breakdowns)
		assert.EqualValues(t, 7*200, resp.Uptime)
		assert.EqualValues(t, 7*100, resp.Downtime)
		assert.EqualValues(t, 200, resp.MTBF.Value)
		assert.EqualValues(t, 100, resp.MTTR.Value)
	})

	agg := types.TimestampedBreakdownAggregate{
		Timestamp:  int64(8) * day,
		MachineID:  mtrn,
		Breakdowns: 3,
		Downtime:   1000,
		Uptime:     0,
	}
	assert.NoError(t, srv.aggrRepo.Store(ctx, agg))

	t.Run("Get KPI Details", func(t *testing.T) {
		path := fmt.Sprintf("/%s/details", mtrn)

		ok, rec := srv.get(t, path, http.StatusOK)
		assert.True(t, ok)

		resp := types.BreakdownDetailsResponse{}
		assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &resp))
		assert.Len(t, resp.Values, 8)

		for _, val := range resp.Values {
			assert.NotZero(t, val.MTTR)
			assert.NotZero(t, val.MTBF)
		}
	})

}

func TestKPITrendAndStatus(t *testing.T) {
	ctx := context.Background()

	srv := newTestServer(uuid.NewV4().String())
	mtrn := trn.NewMachine(srv.CompanyID()).String()

	assert.NoError(t, srv.configRepo.Save(context.Background(), &types.Config{
		MachineID: mtrn,
		ConfigDTO: types.ConfigDTO{
			StatusSensor: "status_sensor",
			MTTRCautionLevel: aws.Float64(150),
			MTBFEmergencyLevel: aws.Float64(1000),
		},
	}))

	now := time.Now()

	t.Run("Post some values", func(t *testing.T) {
		longTimeAgo := now.AddDate(-1,0,0)

		agg := types.TimestampedBreakdownAggregate{
			Timestamp:  longTimeAgo.Unix(),
			MachineID:  mtrn,
			Breakdowns: 1,
			Downtime:   100,
			Uptime:     2000,
		}
		assert.NoError(t, srv.aggrRepo.Store(ctx, agg))

		agg2 := types.TimestampedBreakdownAggregate{
			Timestamp:  now.Unix(),
			MachineID:  mtrn,
			Breakdowns: 10,
			Downtime:   50000,
			Uptime:     2500,
		}
		assert.NoError(t, srv.aggrRepo.Store(ctx, agg2))
	})

	t.Run("Get values", func(t *testing.T) {
		path := fmt.Sprintf("/%s/values/%d", mtrn, now.Unix())

		ok, rec := srv.get(t, path, http.StatusOK)
		assert.True(t, ok)

		resp := types.BreakdownResponse{}
		assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &resp))
		assert.EqualValues(t, 2, resp.MTBF.Status)
		assert.EqualValues(t, calculation.DOWNWARDNEGATIVE, resp.MTBF.Trend)
		assert.EqualValues(t, 1, resp.MTTR.Status)
		assert.EqualValues(t, calculation.UPWARDNEGATIVE, resp.MTTR.Trend)
	})
}
