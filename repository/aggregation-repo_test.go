package repository

import (
	"testing"
	"bitbucket.org/to-increase/go-trn"
	"github.com/stretchr/testify/assert"
	"context"
	"bitbucket.org/to-increase/go-trn/uuid"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"time"
)

func TestStoreAggregate(t *testing.T) {
	ctx := context.Background()

	repo := NewAggregationRepo(localDynamo(), "aggregation-table").(*aggregationRepo)
	repo.model.CreateTable(ctx)

	mtrn := trn.NewMachine(uuid.NewV4().String()).String()

	timestamp := time.Now().Unix()

	aggregate := types.TimestampedBreakdownAggregate{
		MachineID: mtrn,
		Timestamp: timestamp,
	}

	assert.NoError(t, repo.Store(ctx, aggregate))

	t.Run("get the aggregate", func(t *testing.T) {
		aggr, err := repo.Get(ctx, mtrn, timestamp)
		assert.NoError(t, err)
		assert.NotNil(t, aggr)

		t.Run("also at a different timestamp", func(t *testing.T) {
			inOneHour := timestamp + 3600
			aggr2, err := repo.Get(ctx, mtrn, inOneHour)
			assert.NoError(t, err)
			assert.NotNil(t, aggr2)
			assert.Equal(t, aggr2, aggr)
		})
	})
}

func TestGetDetails(t *testing.T) {
	ctx := context.Background()

	repo := NewAggregationRepo(localDynamo(), uuid.NewV4().String()).(*aggregationRepo)
	repo.model.CreateTable(ctx)

	mtrn := trn.NewMachine(uuid.NewV4().String()).String()


	timestamp := time.Now()
	for i := 0; i < 7; i++ {
		ts := timestamp.AddDate(0,0,i)
		assert.NoError(t, repo.Store(ctx, types.TimestampedBreakdownAggregate{
			MachineID: mtrn,
			Timestamp: ts.Unix(),
			Breakdowns: 10,
			Uptime: 20,
			Downtime: 40,
		}))
	}

	endDate := time.Now().AddDate(0,0,6)

	t.Run("get the aggregate", func(t *testing.T) {
		aggr, err := repo.Get(ctx, mtrn, endDate.Unix())
		assert.NoError(t, err)
		assert.NotNil(t, aggr)
		assert.Equal(t, 70, aggr.Breakdowns)
		assert.EqualValues(t, 140, aggr.Uptime)
		assert.EqualValues(t, 280, aggr.Downtime)

	})
	t.Run("get the details", func(t *testing.T) {
		details, err := repo.GetDetails(ctx, mtrn, timestamp.Unix(), endDate.Unix())
		assert.NoError(t, err)
		assert.Len(t, details, 7)
		for _, detail := range details {
			assert.NotZero(t, detail.Breakdowns)
		}
	})

}
