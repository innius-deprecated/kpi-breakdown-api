package main

import (
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/labstack/gommon/log"
	"net/http"
	"time"
	"bitbucket.org/to-increase/go-sns/events/kpis"
)

// swagger:operation POST /{machine_id}/config config postConfig
//
// post kpi
//
// Define the configuration for machine MTTR/MTBF KPIs
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
// - name: body
//   in: body
//   description: the MTTR/MTBF kpi configuration
//   schema:
//     "$ref": "#/definitions/ConfigDTO"
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-breakdown-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the config has been posted
// security:
// - sigv4: []
func (srv *server) postConfig() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		cfg := types.ConfigDTO{}
		if err := json.NewDecoder(r.Body).Decode(&cfg); err != nil {
			log.Printf("Error reading body %+v", err)
			render.Render(w, r, ErrInvalidRequest(err))
			return
		}
		mid := chi.URLParam(r, MachineIDParam)

		existing, err := srv.configRepo.Get(r.Context(), mid)
		if err != nil {
			log.Printf("Error Getting body %+v", err)
			render.Render(w, r, ErrInternalServerError(err))
			return
		}
		if existing != nil && existing.CreatedAt > 0 {
			cfg.CreatedAt = existing.CreatedAt
		} else {
			cfg.CreatedAt = time.Now().Unix()
		}

		if err := srv.configRepo.Save(r.Context(), &types.Config{mid, cfg}); err != nil {
			render.Render(w, r, ErrInternalServerError(err))
			return
		}
		if existing == nil {
			if err := srv.kpiEventPublisher.Publish(r.Context(), kpis.KpiCreatedEvent{MachineID: mid, KPI: "MTTR"}); err != nil {
				log.Errorf("Error posting KPI created event: %+v", err)
			}
			if err := srv.kpiEventPublisher.Publish(r.Context(), kpis.KpiCreatedEvent{MachineID: mid, KPI: "MTBF"}); err != nil {
				log.Errorf("Error posting KPI created event: %+v", err)
			}
		}
		w.WriteHeader(http.StatusOK)
	}
}

// swagger:operation GET /{machine_id}/config config getConfig
//
// get config
//
// Get the configuration for a machine MTTR/MTBF KPI
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-breakdown-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the MTTR/MTBF config
//     schema:
//       "$ref": "#/definitions/ConfigDTO"
// security:
// - sigv4: []
func (srv *server) getConfig() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		mid := chi.URLParam(r, MachineIDParam)

		config, err := srv.configRepo.Get(r.Context(), mid)
		if err != nil {
			render.Render(w, r, ErrInternalServerError(err))
			return
		}

		if config == nil {
			http.Error(w, "", http.StatusNotFound)
			return
		}
		render.Render(w, r, config.ConfigDTO)
	}
}

// swagger:operation DELETE /{machine_id}/config config deleteConfig
//
// delete config
//
// Delete the configuration for a machine MTTR/MTBF KPI
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-breakdown-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the kpi config has been deleted
// security:
// - sigv4: []
func (srv *server) deleteConfig() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		mid := chi.URLParam(r, MachineIDParam)

		err := srv.configRepo.Delete(r.Context(), mid)
		if err != nil {
			http.Error(w, "", http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}
