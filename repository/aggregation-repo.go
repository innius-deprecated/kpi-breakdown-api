package repository

import (
	"bitbucket.org/to-increase/go-trn/uuid"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"bitbucket.org/to-increase/vogels"
	"context"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"time"
	)

type AggregationRepo interface {
	Get(c context.Context, machine string, timestamp int64) (*types.TimestampedBreakdownAggregate, error)
	Getall(c context.Context, company string, timestamp int64) ([]types.TimestampedBreakdownAggregate, error)
	Store(c context.Context, agg types.TimestampedBreakdownAggregate) error

	GetDetails(c context.Context, machine string, fromDate, toDate int64) ([]types.TimestampedBreakdownAggregate, error)

	Delete(c context.Context, machine string) error
}

func NewAggregationRepo(dyndb dynamodbiface.DynamoDBAPI, tablename string) AggregationRepo {
	model := vogels.NewModelV2(dyndb, &vogels.ModelDefinition{
		TableName: tablename,
		Key:       vogels.NewCompositeKey("machine_id", vogels.StringType, "timestamp", vogels.NumberType),
	})
	return &aggregationRepo{model}
}

func NewLocalAggregationRepo(dyndb dynamodbiface.DynamoDBAPI) AggregationRepo {
	repo := NewAggregationRepo(dyndb, uuid.NewV4().String())
	repo.(*aggregationRepo).model.CreateTable(context.Background())
	return repo
}

type aggregationRepo struct {
	model vogels.ModelAPIV2
}

func (a *aggregationRepo) Get(c context.Context, machine string, timestamp int64) (*types.TimestampedBreakdownAggregate, error) {
	dayStart := startOfDay(timestamp)
	aggr := &types.TimestampedBreakdownAggregate{}
	found, err := a.model.GetItem(c, aggr, machine, dayStart)
	if err != nil {
		return nil, err
	}
	if !found {
		//TODO check performance to always perform this query
		return a.lastValueBefore(c, machine, dayStart)
	}
	return aggr, nil
}

func (a *aggregationRepo) Getall(c context.Context, company string, timestamp int64) ([]types.TimestampedBreakdownAggregate, error) {
	queryResults := []types.TimestampedBreakdownAggregate{}

	dayStart := startOfDay(timestamp)
	yesterday := dayStart - 86400

	filter := expression.Name("machine_id").BeginsWith("trn:" + company).
		And(expression.Name("timestamp").Equal(expression.Value(dayStart)).
		Or(expression.Name("timestamp").Equal(expression.Value(yesterday)))).
		And(expression.Name("machine_id").Contains("shift").Not())
	expr, err := expression.NewBuilder().WithFilter(filter).Build()
	if err != nil {
		return nil, err
	}

	if err := a.model.FullScanWithFilter(c, &queryResults, expr); err != nil {
		return nil, err
	}

	response := []types.TimestampedBreakdownAggregate{}
	for _, aggregate := range queryResults {
		if aggregate.Timestamp == dayStart {
			response = append(response, aggregate)
		}
	}
	upper: for _, aggregate := range queryResults {
		if aggregate.Timestamp == yesterday {
			for _, responseAggregate := range response {
				if responseAggregate.MachineID == aggregate.MachineID {
					continue upper
				}
			}
			response = append(response, aggregate)
		}
	}

	return response, err
}

func (a *aggregationRepo) lastValueBefore(c context.Context, machine string, timestamp int64) (*types.TimestampedBreakdownAggregate, error) {
	before := []*types.TimestampedBreakdownAggregate{}
	_, err := a.model.Query(machine).Where("timestamp").LessThan(timestamp).Descending().Limit(1).ExecuteWithContext(c, &before)
	if len(before) > 0 {
		return before[0], err
	}
	return nil, err
}

func (a *aggregationRepo) Store(c context.Context, agg types.TimestampedBreakdownAggregate) error {
	dayStart := startOfDay(agg.Timestamp)

	previousValues, err := a.lastValueBefore(c, agg.MachineID, dayStart)
	if err != nil {
		return err
	}
	if previousValues != nil {
		agg.Breakdowns = previousValues.Breakdowns + agg.Breakdowns
		agg.Downtime = previousValues.Downtime + agg.Downtime
		agg.Uptime = previousValues.Uptime + agg.Uptime
	}
	agg.Timestamp = dayStart

	_, err = a.model.PutItem(c, vogels.NewPutItemRequest(agg))
	return err
}

func (a *aggregationRepo) GetDetails(c context.Context, machine string, fromDate, toDate int64) ([]types.TimestampedBreakdownAggregate, error) {
	results := []types.TimestampedBreakdownAggregate{}
	_, err := a.model.Query(machine).Where("timestamp").Between(startOfDay(fromDate), toDate).ExecuteWithContext(c, &results)
	return results, err
}

func startOfDay(ts int64) int64 {
	t := time.Unix(ts, 0)
	return t.UTC().Truncate(24 * time.Hour).Unix()
}

func (a *aggregationRepo) Delete(c context.Context, machine string) error {
	values := []types.TimestampedBreakdownAggregate{}
	if _, err := a.model.Query(machine).ExecuteWithContext(c, &values); err != nil {
		return err
	}
	for _, v := range values {
		if err := a.model.Destroy(c, v.MachineID, v.Timestamp); err != nil {
			return err
		}
		time.Sleep(100 * time.Millisecond)
	}
	return nil
}
