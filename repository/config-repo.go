package repository

import (
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"context"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"bitbucket.org/to-increase/vogels"
	"bitbucket.org/to-increase/go-trn/uuid"
)

type ConfigRepo interface {
	Get(ctx context.Context, mid string) (*types.Config, error)
	GetAllConfigs(ctx context.Context) ([]types.Config, error)
	Save(ctx context.Context, cfg *types.Config) error
	UpdateLastUpdatedTimestamp(ctx context.Context, machineID string, timestamp int64) error
	Delete(ctx context.Context, mid string) error
}

func NewConfigRepo(dyndb dynamodbiface.DynamoDBAPI, tablename string) ConfigRepo {
	model := vogels.NewModelV2(dyndb, &vogels.ModelDefinition{
		TableName: tablename,
		Key:       vogels.NewKey("id", vogels.StringType),
	})
	return &configRepo{model}
}

func NewLocalConfigRepo(dyndb dynamodbiface.DynamoDBAPI) ConfigRepo {
	repo := NewConfigRepo(dyndb, uuid.NewV4().String())
	repo.(*configRepo).model.CreateTable(context.Background())
	return repo
}

type configRepo struct {
	model vogels.ModelAPIV2
}

func (r *configRepo) Get(ctx context.Context, mid string) (*types.Config, error) {
	cfg := &types.Config{}
	found, err := r.model.GetItem(ctx, cfg, mid, nil)
	if err != nil {
		return nil, err
	}
	if !found {
		return nil, nil
	}
	return cfg, nil
}

func (r *configRepo) GetAllConfigs(ctx context.Context) ([]types.Config, error) {
	cfgs := []types.Config{}
	err := r.model.FullScan(ctx, &cfgs)
	return cfgs, err
}

func (r *configRepo) Save(ctx context.Context, cfg *types.Config) error {
	_, err := r.model.PutItem(ctx, vogels.NewPutItemRequest(*cfg))
	return err
}

func (r *configRepo) UpdateLastUpdatedTimestamp(ctx context.Context, machineID string, timestamp int64) error {
	updated := types.Config{MachineID: machineID}
	updated.LastUpdated = timestamp
	ux, err := vogels.NewUpdateExpressionBuilder(updated).Field("last_updated").Build()
	if err != nil {
		return err
	}
	req := vogels.NewUpdateRequest(updated).WithUpdateExpression(ux)
	_, err = r.model.Update(ctx, req)
	return err
}

func (r *configRepo) Delete(ctx context.Context, mid string) error {
	return r.model.Destroy(ctx, mid, nil)
}