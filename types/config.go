package types

import (
	"net/http"
)

// ConfigDTO is the external representation of an innius MTTR/MTBF kpi configuration
//
// An innius MTTR/MTBF kpi configuration
//
// swagger:model ConfigDTO
type ConfigDTO struct {
	StatusSensor       string   `json:"status_sensor"`
	CreatedAt          int64    `json:"created_at"`
	LastUpdated        int64    `json:"last_updated"` //timestamp of last aggregation
	MTBFCautionLevel   *float64 `json:"mtbf_caution_level"`
	MTBFEmergencyLevel *float64 `json:"mtbf_emergency_level"`
	MTTRCautionLevel   *float64 `json:"mttr_caution_level"`
	MTTREmergencyLevel *float64 `json:"mttr_emergency_level"`
}

func (res ConfigDTO) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

//The internal/repository struct
type Config struct {
	MachineID string `json:"id"`
	ConfigDTO
}
