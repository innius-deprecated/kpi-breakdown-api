module bitbucket.org/to-increase/kpi-breakdown-api

go 1.11

require (
	bitbucket.org/to-increase/analytics-test-database v0.0.0-20170216072320-0d0e5affded7
	bitbucket.org/to-increase/go-auth v1.0.4
	bitbucket.org/to-increase/go-database-connection v1.0.4
	bitbucket.org/to-increase/go-event-queue v0.0.0-20180705101638-f10daa6e8071
	bitbucket.org/to-increase/go-sdk v1.0.28
	bitbucket.org/to-increase/go-sns v1.0.5
	bitbucket.org/to-increase/go-trn v1.0.1
	bitbucket.org/to-increase/vogels v1.0.0
	github.com/akrylysov/algnhsa v0.0.0-20190203201208-70f315bb89bd
	github.com/aws/aws-sdk-go v1.17.12
	github.com/aws/aws-xray-sdk-go v1.0.0-rc.11
	github.com/go-chi/chi v4.0.1+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-openapi/errors v0.19.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/gommon v0.2.8
	github.com/lib/pq v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.0
	github.com/stretchr/testify v1.3.0
	github.com/vrischmann/envconfig v1.1.0
)
