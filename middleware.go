package main

import (
	"context"
	"net/http"
	"strconv"
	"time"

	authtypes "bitbucket.org/to-increase/go-auth/types"
	"bitbucket.org/to-increase/go-sdk/services/machines/interface"
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/kpi-breakdown-api/repository"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"github.com/sirupsen/logrus"
	"github.com/go-chi/chi"
)

// Checks whether the use has the rights to view this machine's KPI
func MachineAccess(machsvc machiniface.MachineAPI) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			mid := chi.URLParam(r, MachineIDParam)
			mtrn, err := trn.Parse(mid)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			claims := authtypes.FromContext(r.Context())
			if claims.CompanyID() != mtrn.CompanyID {
				shared, err := machsvc.CheckMachineAccess(r.Context(), claims.CompanyID(), mid)
				if err != nil {
					logrus.Errorf("could not check shared machine %v; %+v", mid, err)
					http.Error(w, "", http.StatusInternalServerError)
					return
				}
				if !shared {
					http.Error(w, "you don't have access to this machine", http.StatusForbidden)
					return
				}
			}
			next.ServeHTTP(w, r)
		})
	}
}

type tsContextKey string

const timestampContextKey = tsContextKey("timestamp")

// GetTimestampsFromContext returns the query timestamps from the current context
func GetTimestampFromContext(c context.Context) int64 {
	return c.Value(timestampContextKey).(int64)
}

func Timestamp(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		timestamp, err := strconv.ParseInt(chi.URLParam(r, "timestamp"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if timestamp > time.Now().Add(time.Hour).Unix() { //some margin
			http.Error(w, "Cannot calculate in the future", http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), timestampContextKey, timestamp)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

type cfgContextKey string

const configContextKey = cfgContextKey("config")

// GetConfigFromContext returns the KPI config from the current context
func GetConfigFromContext(c context.Context) types.Config {
	return c.Value(configContextKey).(types.Config)
}

// KPIConfigCtx retrieves the KPI config from the repo and stores it in the context
func KPIConfigCtx(repo repository.ConfigRepo) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			mid := chi.URLParam(r, MachineIDParam)
			config, err := repo.Get(r.Context(), mid)
			if err != nil {
				logrus.Errorf("could not get the config for %s; %+v", mid, err)
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
			if config == nil {
				logrus.Warningf("machine %s does not have a kpi config", mid)
				http.Error(w, "breakdown config not found", http.StatusNotFound)
				return
			}

			ctx := context.WithValue(r.Context(), configContextKey, *config)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
