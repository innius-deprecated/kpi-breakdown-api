package main

import (
	"context"
	"bitbucket.org/to-increase/go-event-queue"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"bitbucket.org/to-increase/kpi-breakdown-api/calculation"
	"github.com/pkg/errors"
	kpievents "bitbucket.org/to-increase/go-sdk/services/kpis/events"
)

type Messaging struct {
	Mute bool
	Queue event_queue.DomainEventQueue
}

func (m Messaging) sendMessages(ctx context.Context, events []kpievents.ThresholdCrossedEvent) error {
	if m.Mute {
		return nil
	}
	for _, e := range events {
		evt, err := event_queue.NewRawEventJson(kpievents.ThresholdCrossed, &e)
		if err != nil {
			return errors.Wrap(err, "Failed to convert event to json")
		}
		if err := m.Queue.PublishEventWithContext(ctx, evt); err != nil {
			return err
		}
	}
	return nil
}

func messages(kpiConfig types.Config, aggr types.TimestampedBreakdownAggregate) []kpievents.ThresholdCrossedEvent {
	values := calculation.AsValues(&aggr)
	events := []kpievents.ThresholdCrossedEvent{}

	if status := calculation.MTBFStatus(values.MTBF, kpiConfig); status > 0 {
		events = append(events, kpievents.ThresholdCrossedEvent{
			Machinetrn: kpiConfig.MachineID,
			Kpi:        "MTBF",
			Status:     status,
			Value:      values.MTBF,
		})
	}
	if status := calculation.MTTRStatus(values.MTTR, kpiConfig); status > 0 {
		events = append(events, kpievents.ThresholdCrossedEvent{
			Machinetrn: kpiConfig.MachineID,
			Kpi:        "MTTR",
			Status:     status,
			Value:      values.MTTR,
		})
	}
	return events
}
