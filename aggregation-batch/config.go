package main

import (
	"time"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	)

type Config struct {
	LogLevel   string `envconfig:"default=info"`
	Repository struct {
		DynamoDB struct {
			TableName string
		}
	}
	Aggregation struct {
		DynamoDB struct {
			TableName string
		}
	}
	Messaging struct {
		EventQueue string
	}
	EnvironmentName string
	ServiceName     string

	MachineID       string `envconfig:"optional"`
	CalculationDate int64  `envconfig:"default=-1"`

	Mute bool `envconfig:"default=false"`
}

func startOfDay(timestamp int64) int64 {
	return time.Unix(timestamp, 0).UTC().Truncate(24 * time.Hour).Unix()
}

func getCalculationDates(kpiConfig types.Config, taskConfig Config) (start int64, end int64) {
	taskStartDate := kpiConfig.LastUpdated
	if taskStartDate == 0 {
		taskStartDate = kpiConfig.CreatedAt
	}

	taskEndDate := taskConfig.CalculationDate

	return taskStartDate, taskEndDate
}
