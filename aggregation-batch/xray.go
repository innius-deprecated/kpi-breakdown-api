package main

import (
	"bytes"
	"github.com/pkg/errors"
	"net/http"
	"time"
)

func localIPV4Address() (string, error) {
	client := &http.Client{
		Timeout: 1 * time.Second,
	}
	resp, err := client.Get("http://169.254.169.254/latest/meta-data/local-ipv4")
	if err != nil {
		return "unknown", errors.Wrap(err, "Cannot retrieve ip")
	}
	defer resp.Body.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	return buf.String(), nil
}
