package main

import (
	"bitbucket.org/to-increase/go-sdk/services/machines/interface"
	"bitbucket.org/to-increase/go-sns"
	"bitbucket.org/to-increase/kpi-breakdown-api/repository"
	"bytes"
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

type mockMachineSvc struct {
	machiniface.MachineAPI
}

func (s *mockMachineSvc) CheckMachineAccess(context.Context, string, string) (bool, error) {
	return true, nil
}

type testserver struct {
	*server
	machsvc   *mockMachineSvc
	CompanyId string
}

type mockPub struct{}

func (m mockPub) Publish(context.Context, interface{}) error {
	return nil
}

func newMockPublisher() publisher.Publisher {
	return mockPub{}
}

func (srv *testserver) CompanyID() string {
	return srv.CompanyId
}

func (srv *testserver) get(t *testing.T, path string, expected int) (bool, *httptest.ResponseRecorder) {
	req, err := http.NewRequest("GET", path, nil)
	if err != nil || req == nil {
		t.FailNow()
	}
	w := httptest.NewRecorder()

	srv.ServeHTTP(w, req)
	return assert.Equal(t, expected, w.Code), w
}

func (srv *testserver) post(t *testing.T, path string, body interface{}, expected int) (bool, *httptest.ResponseRecorder) {
	var buf bytes.Buffer
	assert.NoError(t, json.NewEncoder(&buf).Encode(body))

	req, err := http.NewRequest("POST", path, &buf)
	if err != nil || req == nil {
		t.FailNow()
	}
	w := httptest.NewRecorder()

	srv.ServeHTTP(w, req)
	return assert.Equal(t, expected, w.Code), w
}

func localDynamo() dynamodbiface.DynamoDBAPI {
	awsConfig := &aws.Config{}
	sess, _ := session.NewSession(awsConfig.WithEndpoint("http://localhost:8000").WithRegion("us-east-1"))
	return dynamodb.New(sess)
}

func newTestServer(cid string) *testserver {
	machsvc := &mockMachineSvc{}

	srv := &testserver{
		server: &server{
			machsvc:           machsvc,
			configRepo:        repository.NewLocalConfigRepo(localDynamo()),
			aggrRepo:          repository.NewLocalAggregationRepo(localDynamo()),
			kpiEventPublisher: newMockPublisher(),
		},
		machsvc:   machsvc,
		CompanyId: cid,
	}
	srv.routes()
	return srv
}
