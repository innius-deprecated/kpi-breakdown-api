package main

import (
	"bitbucket.org/to-increase/kpi-breakdown-api/calculation"
	"bitbucket.org/to-increase/kpi-breakdown-api/repository"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"context"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"log"
	"net/http"
	"time"
)

// swagger:operation GET /{machine_id}/values/{timestamp} getValues calculate
//
// get MTTR and MTBF
//
// Returns the MTTR and MTBF values and sub-KPIs for the given machine starting at the config-creation date. Trend are derived from the values one month ago
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
// - name: timestamp
//   in: path
//   description: the timestamp for the calculation
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-breakdown-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the KPI values for the specified period
//     schema:
//       "$ref": "#/definitions/BreakdownResponse"
// security:
// - sigv4: []
func (srv *server) calculate(w http.ResponseWriter, r *http.Request) {
	config := GetConfigFromContext(r.Context())
	timestamp := GetTimestampFromContext(r.Context())

	resp, err := calculateValues(r.Context(), srv.aggrRepo, config, timestamp)
	if err != nil {
		log.Printf("calculation error %+v", err)
		http.Error(w, "Error calculating Breakdowns", http.StatusInternalServerError)
		return
	}
	render.Render(w, r, resp)
}

func calculateValues(ctx context.Context, aggrRepo repository.AggregationRepo, config types.Config, timestamp int64) (*types.BreakdownResponse, error) {
	values, err := calculation.Calculate(ctx, aggrRepo, config.MachineID, timestamp)
	if err != nil {
		return nil, err
	}

	oneMonthAgo := time.Unix(timestamp, 0).AddDate(0, -1, 0)

	previousValues, err := calculation.Calculate(ctx, aggrRepo, config.MachineID, oneMonthAgo.Unix())
	if err != nil {
		log.Printf("Error obtaining previous KPI: %+v", err)
	}

	return &types.BreakdownResponse{
		FromDate:   config.CreatedAt,
		ToDate:     timestamp,
		Uptime:     values.Uptime,
		Downtime:   values.Downtime,
		Breakdowns: values.Breakdowns,
		MTBF:       calculation.MTBF(values, previousValues, config),
		MTTR:       calculation.MTTR(values, previousValues, config),
	}, nil
}

// swagger:operation GET /{machine_id}/details getValues calculateDetails
//
// calculate MTTR/MTBF details
//
// Returns the MTTR and MTBF values and sub-KPIs for the given machine for a custom period (UTC) in daily or hourly values
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
// - name: date_from
//   in: path
//   description: the start timestamp for the calculation
//   type: string
//   required: true
// - name: date_to
//   in: path
//   description: the end timestamp for the calculation
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-breakdown-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the MTTR/MTBF values for the specified period
//     schema:
//       type: array
//       items:
//         "$ref": "#/definitions/BreakdownDetailsResponse"
// security:
// - sigv4: []
func (srv *server) calculateDetails(w http.ResponseWriter, r *http.Request) {
	config := GetConfigFromContext(r.Context())

	resp, err := calculateDetailedValues(r.Context(), srv.aggrRepo, config.MachineID, config.CreatedAt, time.Now().Unix())
	if err != nil {
		log.Printf("calculation KPI details error %+v", err)
		http.Error(w, "Error calculating KPI details", http.StatusInternalServerError)
		return
	}
	render.Render(w, r, resp)
}

func calculateDetailedValues(ctx context.Context, aggrRepo repository.AggregationRepo, mid string, from, to int64) (types.BreakdownDetailsResponse, error) {
	values, err := calculation.CalculateDetails(ctx, aggrRepo, mid, from, to)
	return types.BreakdownDetailsResponse{
		Values:   values,
		FromDate: from,
		ToDate:   to,
	}, err
}

// swagger:operation GET /all/{company_id}/{timestamp} getBreakdown calculateForAllMachines
//
// get breakdown
//
// Returns the breakdown values for all machines for a custom period (UTC)
//
// ---
//
// parameters:
// - name: company_id
//   in: path
//   description: the company id (trn)
//   type: string
//   required: true
// - name: timestamp
//   in: path
//   description: the (end) timestamp for the calculation
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-breakdown-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the breakdown values for all machines in the specified period
//     schema:
//       type: array
//       items:
//         "$ref": "#/definitions/BreakdownResponse"
// security:
// - sigv4: []
func (srv *server) calculateForAllMachines(w http.ResponseWriter, r *http.Request) {
	cid := chi.URLParam(r, CompanyIDParam)

	timestamp := GetTimestampFromContext(r.Context())

	values, err := calculation.CalculateForAllMachines(r.Context(), srv.aggrRepo, cid, timestamp)
	if err != nil {
		log.Printf("calculation breakdown error %+v", err)
		http.Error(w, "Error calculating breakdown kpi's", http.StatusInternalServerError)
		return
	}

	resp := []render.Renderer{}
	for machineID, val := range values {
		resp = append(resp, &types.BreakdownResponse{
			MachineId:  machineID,
			ToDate:     timestamp,
			MTTR:       types.BreakdownKPI{Value: val.MTTR},
			MTBF:       types.BreakdownKPI{Value: val.MTBF},
			Uptime:     val.Uptime,
			Downtime:   val.Downtime,
			Breakdowns: val.Breakdowns,
		})
	}

	render.RenderList(w, r, resp)
}
