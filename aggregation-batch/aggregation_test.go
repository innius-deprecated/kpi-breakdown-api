package main

import (
	"testing"
	"bitbucket.org/to-increase/go-trn/uuid"
	"github.com/stretchr/testify/assert"
)

func TestAggregate(t *testing.T) {
	mtrn := uuid.NewV4().String()

	t.Run("Count breakdowns, up- and downtime", func(t *testing.T) {
		queryResults := []QueryResult{
			{Starttime: 0, Seconds: 10, Value: 0},  //10 sec OFF
			{Starttime: 10, Seconds: 10, Value: 1}, //10 sec ON
			{Starttime: 20, Seconds: 40, Value: 2}, //40 sec ERROR
			{Starttime: 60, Seconds: 10, Value: 1}, //10 sec ON
			{Starttime: 70, Seconds: 15, Value: 3}, //15 sec ERROR
		}
		aggr := aggregate(mtrn, 0, queryResults)
		assert.Equal(t, 2, aggr.Breakdowns)
		assert.EqualValues(t, 20, aggr.Uptime)
		assert.EqualValues(t, 55, aggr.Downtime)
	})

	t.Run("Do not count breakdown at start", func(t *testing.T) {
		queryResults := []QueryResult{
			{Starttime: 0, Seconds: 10, Value: 3},  //10 sec ERROR
			{Starttime: 10, Seconds: 10, Value: 1}, //10 sec ON
			{Starttime: 20, Seconds: 40, Value: 0}, //40 sec OFF
			{Starttime: 60, Seconds: 10, Value: 1}, //10 sec ON
			{Starttime: 70, Seconds: 15, Value: 3}, //15 sec ERROR
		}
		aggr := aggregate(mtrn, 0, queryResults)
		assert.Equal(t, 1, aggr.Breakdowns)
		assert.EqualValues(t, 20, aggr.Uptime)
		assert.EqualValues(t, 25, aggr.Downtime)
	})
}
