package calculation

import (
	"testing"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"github.com/stretchr/testify/assert"
	"github.com/aws/aws-sdk-go/aws"
)

func TestMTBF(t *testing.T) {
	values := types.BreakdownValue{
		MTBF: 20,
	}
	previousValues := types.BreakdownValue{
		MTBF: 30,
	}

	config := types.Config{}
	config.MTBFCautionLevel = aws.Float64(25)

	kpi := MTBF(values, previousValues, config)
	assert.Equal(t, 4, kpi.Trend)
	assert.Equal(t, 1, kpi.Status)
}

func TestMTTR(t *testing.T) {
	values := types.BreakdownValue{
		MTTR: 20,
	}
	previousValues := types.BreakdownValue{
		MTTR: 30,
	}

	config := types.Config{}
	config.MTTREmergencyLevel = aws.Float64(15)

	kpi := MTTR(values, previousValues, config)
	assert.Equal(t, 3, kpi.Trend)
	assert.Equal(t, 2, kpi.Status)
}
