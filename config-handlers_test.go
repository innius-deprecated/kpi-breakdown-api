package main

import (
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/go-trn/uuid"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
	"net/http/httptest"
	"time"
)

func TestPostConfig(t *testing.T) {
	//ctx := context.Background()

	srv := newTestServer(uuid.NewV4().String())
	mtrn := trn.NewMachine(srv.CompanyID()).String()
	path := fmt.Sprintf("/%s/config", mtrn)

	config := types.ConfigDTO{
		StatusSensor:       "foo",
		MTBFEmergencyLevel: aws.Float64(58.3),
	}

	t.Run("Post config", func(t *testing.T) {
		srv.post(t, path, config, http.StatusOK)
	})

	t.Run("Get config", func(t *testing.T) {
		_, rec := srv.get(t, path, http.StatusOK)

		resp := types.ConfigDTO{}
		assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &resp))
		assert.EqualValues(t, config.StatusSensor, resp.StatusSensor)
		assert.EqualValues(t, config.MTBFEmergencyLevel, resp.MTBFEmergencyLevel)
		assert.NotZero(t, resp.CreatedAt)

		t.Run("Updating should not change creation date", func(t *testing.T) {
			updated := config
			updated.StatusSensor = "bar"
			srv.post(t, path, updated, http.StatusOK)

			_, rec2 := srv.get(t, path, http.StatusOK)
			resp2 := types.ConfigDTO{}
			assert.NoError(t, json.Unmarshal(rec2.Body.Bytes(), &resp2))
			assert.EqualValues(t, "bar", resp2.StatusSensor)
			assert.EqualValues(t, resp.CreatedAt, resp2.CreatedAt)

		})
	})

	t.Run("Delete config", func(t *testing.T) {
		req, err := http.NewRequest("DELETE", path, nil)
		assert.NoError(t, err)
		w := httptest.NewRecorder()
		srv.ServeHTTP(w, req)
		assert.Equal(t, http.StatusOK, w.Code)

		t.Run("Config is removed", func(t *testing.T) {
			srv.get(t, path, http.StatusNotFound)

			valuePath := fmt.Sprintf("/%s/values/%d", mtrn, time.Now().Unix())
			srv.get(t, valuePath, http.StatusNotFound)
		})
	})
}
