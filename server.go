package main

import (
	"bitbucket.org/to-increase/go-sdk/services/machines"
	"bitbucket.org/to-increase/go-sdk/services/machines/interface"
	"bitbucket.org/to-increase/go-sns"
	"bitbucket.org/to-increase/go-sns/subscriber"
	machine_events "bitbucket.org/to-increase/kpi-breakdown-api/events/subscriptions/machines"
	"bitbucket.org/to-increase/kpi-breakdown-api/repository"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/ecs"
	"github.com/aws/aws-sdk-go/service/ecs/ecsiface"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/go-chi/chi"
	"net/http"
)

type server struct {
	chi.Router
	config     Config
	configRepo repository.ConfigRepo
	aggrRepo   repository.AggregationRepo
	ecsClient  ecsiface.ECSAPI
	machsvc    machiniface.MachineAPI

	machineEventSubscr  subscriber.Subscriber
	machineEventHandler http.HandlerFunc
	kpiEventPublisher   publisher.Publisher
}

func NewServer(conf Config) (*server, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}

	db := dynamodb.New(sess)
	xray.AWS(db.Client)

	snsClient := sns.New(sess)

	subscr := subscriber.NewSubscriber(snsClient, conf.MachineEventSNSTopic, conf.ExternalServiceEndpoint+"/sns/machine_events")
	kpiPublisher := publisher.NewPublisher(snsClient, conf.KpiEventSNSTopic)

	repo := repository.NewConfigRepo(db, conf.Repository.DynamoDB.TableName)
	aggregationRepo := repository.NewAggregationRepo(db, conf.Aggregation.DynamoDB.TableName)

	srv := &server{
		config:     conf,
		configRepo: repository.NewConfigRepo(db, conf.Repository.DynamoDB.TableName),
		aggrRepo:   repository.NewAggregationRepo(db, conf.Aggregation.DynamoDB.TableName),
		ecsClient:  ecs.New(sess),
		machsvc:    machines.New(),

		machineEventSubscr:  subscr,
		machineEventHandler: subscr.HttpHandler(machine_events.New(repo, aggregationRepo, kpiPublisher)),
		kpiEventPublisher:   kpiPublisher,
	}

	return srv, nil
}
