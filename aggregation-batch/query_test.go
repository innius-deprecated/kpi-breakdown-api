package main

import (
	"bitbucket.org/to-increase/analytics-test-database"
	"bitbucket.org/to-increase/go-trn/uuid"
	"bitbucket.org/to-increase/kpi-breakdown-api/types"
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
)

type mockQuerySystem struct {
	QuerySystem
}

func TestQuery(t *testing.T) {
	mtrn := uuid.NewV4().String()

	statusSensor := uuid.NewV4().String()
	start := int64(0)
	end := start + 86400
	values := []float32{0,1,2,1,1,2,0,1,4,4,4,4,4} //Blocks of 2 hours

	tdb, _ := testdb.NewLocalAnalyticsDatabase()

	qs, err := newMockQuerySystem()
	assert.NoError(t, err)
	assert.NotNil(t, qs)

	assert.NoError(t, tdb.InsertSensorDataSeries(mtrn, statusSensor, start, end, values))

	config := types.Config{
		MachineID: mtrn,
		ConfigDTO: types.ConfigDTO{
			StatusSensor: statusSensor,
		},
	}

	results, err := qs.Query(context.Background(), config, start, end)
	assert.NoError(t, err)
	assert.Len(t, results, 8)

	t.Run("Test aggregation of results", func(t *testing.T) {
		aggr := aggregate(mtrn, start, results)
		assert.Equal(t, 3, aggr.Breakdowns)
		assert.EqualValues(t, 12 * 3600, aggr.Downtime)
		assert.EqualValues(t, 8 * 3600, aggr.Uptime )
	})

	t.Run("The next day should be all the same status", func(t *testing.T) {
		nextDay := end + 86400

		rrr, err := qs.Query(context.Background(), config, nextDay, nextDay + 86400)
		assert.NoError(t, err)
		assert.Len(t, rrr, 1)
		assert.EqualValues(t, 4, rrr[0].Value)

		aggr := aggregate(mtrn, nextDay, rrr)
		assert.Equal(t, 0, aggr.Breakdowns)
		assert.EqualValues(t, 24 * 3600, aggr.Downtime)
		assert.EqualValues(t, 0, aggr.Uptime)
	})
}
