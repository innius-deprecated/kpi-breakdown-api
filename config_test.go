package main

import (
	"testing"
	"os"
	"github.com/stretchr/testify/assert"
	"github.com/vrischmann/envconfig"
)

func TestConfig(t *testing.T) {
	os.Setenv("ENVIRONMENT_NAME", "chair")
	os.Setenv("SERVICE_NAME", "kpi-breakdown-api")
	os.Setenv("REPOSITORY_DYNAMODB_TABLENAME", "table1")
	os.Setenv("AGGREGATION_DYNAMODB_TABLENAME", "table2")
	os.Setenv("BASE_PATH", "/")
	os.Setenv("AGGREGATEBATCH_TASKDEFINITION", "task")
	os.Setenv("AGGREGATEBATCH_CONTAINERNAME", "task")
	os.Setenv("MACHINE_EVENT_SNS_TOPIC", "topic")
	os.Setenv("EXTERNAL_SERVICE_ENDPOINT", "https://api.test.innius.com/breakdown")
	os.Setenv("KPI_EVENT_SNS_TOPIC", "topic")

	conf := Config{}
	assert.NoError(t, envconfig.Init(&conf))
}
