package calculation

import "bitbucket.org/to-increase/kpi-breakdown-api/types"

const (
	NEUTRAL = iota
	UPWARDPOSITIVE
	UPWARDNEGATIVE
	DOWNWARDPOSITIVE
	DOWNWARDNEGATIVE
)

// For MTBF, higher values are better
func MTBF(values, previousValues types.BreakdownValue, config types.Config) types.BreakdownKPI {
	return types.BreakdownKPI{
		Value:     values.MTBF,
		Caution:   config.MTBFCautionLevel,
		Emergency: config.MTBFEmergencyLevel,
		Status:    MTBFStatus(values.MTBF, config),
		Trend:     mtbfTrend(values.MTBF, previousValues.MTBF),
	}
}

func MTBFStatus(value float64, config types.Config) int {
	if config.MTBFEmergencyLevel != nil && value < *config.MTBFEmergencyLevel {
		return 2
	}
	if config.MTBFCautionLevel != nil && value < *config.MTBFCautionLevel {
		return 1
	}
	return 0
}

func mtbfTrend(value, previousValue float64) int {
	if value > previousValue {
		return UPWARDPOSITIVE
	}
	if value < previousValue {
		return DOWNWARDNEGATIVE
	}
	return NEUTRAL
}

// For MTTR, lower values are better
func MTTR(values, previousValues types.BreakdownValue, config types.Config) types.BreakdownKPI {
	return types.BreakdownKPI{
		Value:     values.MTTR,
		Caution:   config.MTTRCautionLevel,
		Emergency: config.MTTREmergencyLevel,
		Status:    MTTRStatus(values.MTTR, config),
		Trend:     mttrTrend(values.MTTR, previousValues.MTTR),
	}
}

func MTTRStatus(value float64, config types.Config) int {
	if config.MTTREmergencyLevel != nil && value > *config.MTTREmergencyLevel {
		return 2
	}
	if config.MTTRCautionLevel != nil && value > *config.MTTRCautionLevel {
		return 1
	}
	return 0
}

func mttrTrend(value, previousValue float64) int {
	if value > previousValue {
		return UPWARDNEGATIVE
	}
	if value < previousValue {
		return DOWNWARDPOSITIVE
	}
	return NEUTRAL
}
